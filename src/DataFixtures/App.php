<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class App extends Fixture
{
  private $passwordEncoder;

  public function __construct(UserPasswordEncoderInterface $passwordEncoder)
  {
    $this->passwordEncoder = $passwordEncoder;
  }
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
      $account = new User();
      $account->setEmail("william@codecolliders.com");
      $account->setPassword($this->passwordEncoder->encodePassword($account, "password"));
      $account->setAdmin(true);
      $manager->persist($account);
        $manager->flush();
    }
}
