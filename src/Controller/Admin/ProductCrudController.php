<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpKernel\KernelInterface;

class ProductCrudController extends AbstractCrudController
{

  private $rootDir;
  /**
   * ProductCrudController constructor.
   */
  public function __construct(KernelInterface $kernel)
  {
    $this->rootDir =$kernel->getProjectDir();
  }

  public static function getEntityFqcn(): string
    {
        return Product::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
          TextField::new("imageSrc")/*->setUploadDir($this->rootDir."/public/uploads")->setBasePath("/uploads")*/,
          TextField::new("name"),
          TextField::new("reference"),
          NumberField::new("price"),
          TextEditorField::new("description"),
          NumberField::new("stock"),
        ];
    }

}
