<?php

namespace App\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\Order;
use App\Entity\OrderLine;
use App\Entity\User;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Service\CartManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Validator\Constraints;

class UserController extends AbstractController
{
  /**
   * @Route("/api/add-to-cart", name="add_to_cart", methods={"PUT"})
   */
  public function addToCart(Request $request, CartManager $cartManager, DecoderInterface $decoder, ProductRepository $repository): Response
  {
    try {
      $data = $decoder->decode($request->getContent(), "json");
    } catch (Exception $e) {
      return $this->json(["error" => "invalid JSON"], 400);
    }
    if (empty($data["product"])) {
      return $this->json(["error" => "parameter product is required"], 400);
    }
    $product = $repository->find($data["product"]);
    if (!$product) {
      return $this->json(["error" => "product not found"], 400);
    }
    $cart = $cartManager->getCart();
    $line = new OrderLine();
    $line->setQuantity(isset($data["quantity"]) ?$data["quantity"]: 1);
    $line->setCurrentOrder($cart);
    $line->setProduct($product);
    $em = $this->getDoctrine()->getManager();
    $em->persist($line);
    $em->flush();
    return $this->json($cart, 201, [], ["groups" => "order.show"]);
  }


  /**
   * @Route("/api/get-cart", name="get_cart", methods={"GET"})
   */
  public function getCart(CartManager $cartManager)
  {
    $cart = $cartManager->getCart();
    return $this->json($cart, 200, [], ["groups" => "order.show"]);
  }

  /**
   * @Route("/api/account", name="user_account", methods={"GET"})
   */
  public function getUserData()
  {
    return $this->json($this->getUser(), 200, [], ["groups" => "user.show"]);
  }

  /**
   * @Route("/api/orders", name="user_orders", methods={"GET"})
   */
  public function getOrders(OrderRepository $repository)
  {
    $user = $this->getUser();
    $orders = $repository->getHistory($user);
    return $this->json($orders, 200, [], ["groups" => "order.list"]);
  }

  /**
   * @Route("/api/orders/{order}", name="user_order", methods={"GET"})
   */
  public function getOrder(Order $order)
  {
    $user = $this->getUser();
    if ($order->getUser() !== $user){
      return $this->json(["error" => "unauthorized"], 401);
    }
    return $this->json($order, 200, [], ["groups" => "order.show"]);
  }



  /**
   * @Route("/api/confirm-cart", name="confirm-cart", methods={"PUT"})
   */
  public function confirmCart(CartManager $cartManager){
    $cart = $cartManager->getCart();
    if(sizeof($cart->getOrderLines()->toArray()) === 0){
      return $this->json(["error" => "Cart is empty"], 400);
    }
    $errors = $cartManager->updateStocks($cart);
    if(sizeof($errors)> 0){
      return $this->json(["error" => "Not enough stock", "invalid_products" => $errors], 400, [], ["groups" => "product.list"]);
    }
    $cart->setStatus(Order::PROCESSING);
    $this->getDoctrine()->getManager()->flush();

    return $this->json($cart, 200, [], ["groups" => "product.list"]);
  }

  /**
   * @Route("/api/register", name="register", methods={"POST"})
   */
  public function register(Request $request, DecoderInterface $decoder, UserPasswordEncoderInterface $encoder)
  {
    try {
      $data = $decoder->decode($request->getContent(), "json");
    } catch (Exception $e) {
      return $this->json(["error" => "invalid JSON"], 400);
    }
    if (empty($data["email"])) {
      return $this->json(["error" => "parameter email is required"], 400);
    }
    if (empty($data["password"])) {
      return $this->json(["error" => "parameter password is required"], 400);
    }

    $user = new User();
    $user->setEmail($data["email"]);
    $user->setPassword($encoder->encodePassword($user, $data["password"]));
    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();

    return $this->json($user, 200, [], ["groups" => "user.show"]);
  }
}
