<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
  const CART = "CART";
  const PROCESSING = "PROCESSING";
  const CANCELED = "CANCELED";
  const SENT = "SENT";

  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   * @Groups({"order.show","product.list", "order.list"})
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   * @Groups({"order.show","product.list", "order.list"})
   */
  private $status;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   * @Groups({"order.show","product.list"})
   */
  private $number;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   * @Groups({"order.show","product.list", "order.list"})
   */
  private $date;

  /**
   * @ORM\OneToMany(targetEntity=OrderLine::class, mappedBy="currentOrder", orphanRemoval=true)
   * @Groups({"order.show"})
   */
  private $orderLines;

  /**
   * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
   * @ORM\JoinColumn(nullable=false)
   * @Groups({"order.show"})
   */
  private $user;

  public function __construct()
  {
    $this->orderLines = new ArrayCollection();
    $this->setStatus(self::CART);
  }

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getStatus(): ?string
  {
    return $this->status;
  }

  public function setStatus(?string $status): self
  {
    $this->status = $status;

    return $this;
  }

  public function getNumber(): ?string
  {
    return $this->number;
  }

  public function setNumber(?string $number): self
  {
    $this->number = $number;

    return $this;
  }

  public function getDate(): ?DateTimeInterface
  {
    return $this->date;
  }

  public function setDate(?DateTimeInterface $date): self
  {
    $this->date = $date;

    return $this;
  }

  public function addOrderLine(OrderLine $orderLine): self
  {
    if (!$this->orderLines->contains($orderLine)) {
      $this->orderLines[] = $orderLine;
      $orderLine->setCurrentOrder($this);
    }

    return $this;
  }

  public function removeOrderLine(OrderLine $orderLine): self
  {
    if ($this->orderLines->removeElement($orderLine)) {
      // set the owning side to null (unless already changed)
      if ($orderLine->getCurrentOrder() === $this) {
        $orderLine->setCurrentOrder(null);
      }
    }

    return $this;
  }

  public function getUser(): ?User
  {
    return $this->user;
  }

  public function setUser(?User $user): self
  {
    $this->user = $user;

    return $this;
  }

  /**
   * @return mixed
   * @Groups({"order.show", "order.list"})
   */
  public function getTotal()
  {
    return array_reduce($this->getOrderLines()->toArray(), function ($cumulator, $order) {
      return $order->getTotal() + $cumulator;
    }, 0);
  }

  /**
   * @return Collection|OrderLine[]
   */
  public function getOrderLines(): Collection
  {
    return $this->orderLines;
  }
}
