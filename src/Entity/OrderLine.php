<?php

namespace App\Entity;

use App\Repository\OrderLineRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OrderLineRepository::class)
 */
class OrderLine
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   * @Groups({"order.show"})
   */
  private $id;

  /**
   * @ORM\Column(type="integer")
   * @Groups({"order.show"})
   */
  private $quantity;

  /**
   * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="orders")
   * @ORM\JoinColumn(nullable=false)
   * @Groups({"order.show"})
   */
  private $product;

  /**
   * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderLines")
   * @ORM\JoinColumn(nullable=false)
   */
  private $currentOrder;

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getCurrentOrder(): ?Order
  {
    return $this->currentOrder;
  }

  public function setCurrentOrder(?Order $currentOrder): self
  {
    $this->currentOrder = $currentOrder;

    return $this;
  }

  /**
   * @return float|int
   * @Groups({"order.show"})
   */
  public function getTotal()
  {
    return $this->getProduct()->getPrice() * $this->getQuantity();
  }

  public function getProduct(): ?Product
  {
    return $this->product;
  }

  public function setProduct(?Product $product): self
  {
    $this->product = $product;

    return $this;
  }

  public function getQuantity(): ?int
  {
    return $this->quantity;
  }

  public function setQuantity(int $quantity): self
  {
    $this->quantity = $quantity;

    return $this;
  }
}
